SSH Module
==========

The module installs ssh server and optionally client packages, manages sshd server and ssh client system-wide configuration files. The module does not directly depend on any other, but without the following modules the resulting authentication configuration may be unuseable:

* `pam` - Must be present on the system to have a useable configuration. It is important to keep the homonymous parameters of `ssh` and `pam` modules in sync.
* `duo` - If you want a DUO support, this module must be present. ssh module does not attempt to figure out if `duo` class is present because such detection in Puppet is very unreliable. It is up to you to make sure that `enable_duo` parameter is disabled if you are not planning to use DUO second factor authentication.
* `su_kerberos` - If you need GSSAPI support, this module also must be present on the system. Not necessary if you plan to use only public key authentication. Again, if the latter is the case, you must set `gssapi` parameter in `ssh` module to `false`.
* `afs` - To obtain AFS tokens at login time, AFS file system has to be present. Note that if GSSAPI authentication is enabled, pam_afs_session.so module must be installed in any case, even if you don't want AFS. Othewise kerberos password and ticket handling would break. 

## How module works and basic assumptions

This modules ranks the authentication methods from most to least preferable as follows:

1. GSSAPI
2. Public key
3. Password

Each of the authentication methods can be independently enabled or disabled. If an athentication method is disabled, then next preferable becomes default. For instance, by default GSSAPI is enabled and is used by default. If you disable GSSAPI in the module parameters, public key authentication becomes default.
 
These authentication methods are applied to the following types of users:

* Default standard user. Any user with a valid SUNet ID without any special exceptions applied.
* Standard user with exceptions applied. The module relies on a number of local UNIX groups to require special authentication methods to such group. For instance, if GSSAPI method is default, but you want a particular user account to authenticate with a public key, you can assign a user to a particular local group and that would make public key authentication required for him. Such exceptions are needed only when DUO second factor authentication is enabled.
* Administrative user. The user allowed to elevate his permissions to root. In case of GSSAPI authentication such users must use separate Kerberos credentials. This type of user account is needed only if DUO second factor authentication is enabled.
* Root user. The actual root user. By default direct logins to root account are disabled for security reasons. If your use case requires it, you can enable them.

## Parameters

The list of available parameters and general directions on how and when to use them.

### Authentication options

In this section the parameters defining authentication methods in general are listed. The module allows to make exceptions on per-user basis, but it is important that even for the users falling under an exception rule for an authentication method, this authentication method must be made generally available in the first place. 

##### enable duo

Enable support for DUO authentication in general. The module allows to create exceptions from second factor authentictation on per-user basis. Such exceptions may be needed, for instance, to allow shell logins or SFTP connections from other servers or devices.

##### pubkey

Enable public key authentication. The module disables public key authentication for standard users if there is a better alternative, GSSAPI in particular.

##### password

Enables password authentication. Necessary for traditional password authentication and desireable in most cases in conjunction with GSSAPI authentication. Without password authentication enabled users would be forced to use exclusively their Kerberos tickets and would never be prompted for a password if they don't have a ticket. It makes sense to disable password authentication in general only if public key is the only desirable way of authentication.

##### gssapi

Enables GSSAPI authentication support. This is a preferred method of authentication for standard and administrative users and is set to be the only method if available.

##### allow_root

Allows remote logins as `root` user. In most cases it is not desirable, but there might be a use cases when an automation running on a different server must execute privileged commands. The parameter can have values `false`, `gssapi`, `pubkey` and `password` denoting the type of the authentication enforced for root account. Of couse, before setting authentication method make sure that it is generally enabled by the corresponding parameter given above. DUO second factor authentication is not used for root account. `password` authentication for `root` user becomes available only if better authentication methods i.e. `gssapi` and `pubkey` are disabled.

### Other options

The rest of the sshd configuration options.

##### x11forwarding

Enables X11 forwarding over ssh connection.

##### max_root_sessions

Maximum number of simultaneously opened root sessions.

##### listen_addresses

Array of local IP addresses, where sshd should be listening. By default it listens on all.

##### listen_ports

Array of local ports, where sshd should be listening. By default it listens only on port 22.

##### install_client

Specifies if ssh client has to be installed.

##### sftp_settings

A hash of Match rules for SFTP connections. You can use either default authentication options for SFTP usres, as they are defined by parameters in the previous section or specify custiom ones:

```yaml
ssh::sftp_settings:
  'Host appz.stanford.edu':
    ChrootDirectory: '/srv/files'
    PubkeyAuthentication: 'on'
```

##### extra_settings

Any settings which are not already configuratble with the parameters of the module. A hash of key/value pairs.

##### client_extra_settings

Any custom settings for the default ssh client configuration. A hash of key/value pairs.

##### custom_template

If your sshd configuration is not supported by this module, you can specify a custom template to be used instead.

## Which configuration options to use?

It is possible to create a lot of configurations by using general authentication options together with exceptions. Some of them would probably be not practical and have a different level of security. In this section there is a recommended list of most common configurations. All of them assume that .k5login files and autorized_keys files are created and put in the right places where appropriate. `users` module would do that for you automatically.

1. GSSAPI authentication for standard and administrative users with mandatory DUO second factor. No public key authentication for anyone.

```yaml
ssh::enable_duo: true
ssh::gssapi: true
ssh::password: true
ssh::pubkey: false
ssh::allow_root: false
```

2. GSSAPI authentication only for the systems not supporting DUO authentication or low risk systems, where it is an unnecessary burden.

```yaml
ssh::enable_duo: false
ssh::gssapi: true
ssh::password: true
ssh::pubkey: false
ssh::allow_root: true
```

  In this case administrators typically login directly as `root` account with a special kerberos _root princial_. To achive that .k5login file with the list of administartor's root principals is put into `/root` directory. If you do not use root principlas for administration, it is better to set `allow_root` to `false` and allow administrators login as regular users. To allow administrators to elevate privileges, create a sudo rule for them.

3. Public key authentication with DUO second factor:

```yaml
ssh::enable_duo: true
ssh::gssapi: false
ssh::password: false
ssh::pubkey: true
ssh::allow_root: false
```

4. Public key authentication without DUO second factor:

```yaml
ssh::enable_duo: false
ssh::gssapi: false
ssh::password: false
ssh::pubkey: true
ssh::allow_root: false
```

If a direct login for the root account is desirable, set `ssh::allow_root` parameter to `pubkey` or `gssapi` in any of these configurations.

## Defining different authentication methods for groups of users

The module uses a special set of local groups to flexibly control authentication methods when DUO second factor authentication is enabled. The groups are:

* `gssapi_noduo` - Members of this group must authenticate with GSSAPI, but do not get DUO second factor prompt.
* `pubkey_noduo` - Members of this group must authenticate with public key, but do not provide second factor to DUO.
* `pubkey_duo` - Members of this group must authenticate with public key and DUO.
* `passwd_noduo` - Members of this group must authenticate with local password and do not provide DUO second factor.

Assigning a user to one and _only one_ of these groups would make the module require one of these kinds of authentication different from the default authentication scheme. Membership in a particular group is considered only if a corresponding authentication method is enabled in general. For instance, if `ssh::pubkey` is set to `false`, membership in groups `pubkey_duo` and `pubkey_noduo` would not be evaluated.

## Examples

```yaml
ssh::enable_duo: true
ssh::pubkey: false
ssh::password: true
ssh::gssapi: true
ssh::allow_root: false
ssh::listen_address:
  - '171.64.1.1'
ssh::listen_ports:
  - 22
  - 2222
ssh::extra_settings:
  AllowGroups: 'admins stem-workgroup'
ssh::sftp_settings:
  'Group sftpusers':
    ChrootDirectory: '/srv/dropbox'
    PubkeyIAuthentication: false
```

