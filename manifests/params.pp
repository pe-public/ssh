
class ssh::params {

  $auth_groups = {
    pubkey_no_duo   => 'pubkey_noduo',
    pubkey_with_duo => 'pubkey_duo',
    gssapi_no_duo   => 'gssapi_noduo',
    password_no_duo => 'passwd_noduo'
  }
}

