# Class installs and configures ssh client and server.
# Does not require any other module to work, but any useable configuration
# requires pam module. If @gssapi or @enable_duo are set to true, respectively
# depends on su_kerberos and duo modules.
#
# Depends on: pam, [su_kerberos], [duo]
#
# Parameters:
#
# @enable_duo            Configure sshd for duo authentication
# @pubkey                Enable pubkey authentication
# @password              Allow password authentication
# @gssapi                Enable GSSAPI authentication
# @allow_root            Allow direct logins to root account
# @x11forwarding         Allow X11 forwarding
# @max_tries             Maximum number of login attemps allowed
# @max_root_sessions     Maximum number of allowed root sessions
# @listen_address        IP addresses where sshd daemon listens on
# @listen_ports          Ports which sshd daemon is listening on
# @install_client        True if ssh client should be installed
# @sftp_settings         Sshd configuration for SFTP connections.
# @extra_settings        Extra ssh settings in a key => value format
# @custom_template       Custom puppet template for sshd service.
#                        If you have a one-off configuration which requires
#                        sshd configuration not supported by this module.
# @root_name_pattern     The account names for administrative users, who can
#                        elevate their privileges with no password by default,
#                        are derived using a regex pattern. The pattern must 
#                        contain a backreference '\0', which gets
#                        substituted with a sunet id of a user. For instance,
# 
# Hiera example:
# 
#   ssh::enable_duo: true
#   ssh::pubkey: false
#   ssh::password: true
#   ssh::gssapi: true
#   ssh::allow_root: false
#   ssh::listen_address:
#     - '171.64.1.1'
#   ssh::listen_ports:
#     - 22
#     - 2222
#   ssh::extra_settings:
#     AllowGroups: 'admins stem-workgroup'
#   ssh::sftp_settings:
#     'Host appz.stanford.edu':
#       ChrootDirectory: '/srv/files'
#       PubkeyIAuthentication: true
#       

class ssh (
  Boolean $enable_duo = true,
  Boolean $pubkey = false,
  Variant[Boolean[false], Enum['pubkey', 'gssapi', 'password']] $allow_root = false,
  Boolean $password = true,
  Boolean $x11forwarding = true,
  Integer $max_root_sessions = 5,
  Boolean $install_client = true,
  Boolean $gssapi = true,
  String $root_name_pattern = 'root.\0',
  Hash[String,String] $auth_groups = {},
  Optional[Array[Stdlib::IP::Address]] $listen_addresses = undef,
  Optional[Array[Stdlib::Port]] $listen_ports = undef,
  Optional[Hash[String, String]] $extra_settings = undef,
  Optional[Hash[String, String]] $client_extra_settings = undef,
  Optional[Hash[String, Hash]] $sftp_settings = undef,
  Optional[String] $custom_template = undef
) {
  include ssh::params

  # override authentication group names, if needed.
  $auth_groups_real = $::ssh::params::auth_groups + $auth_groups

  # format check for a root user name pattern
  if $root_name_pattern !~ /\\+0/ {
    fail('Root user name pattern must include a "\0" backreference.')
  }

  # Custom administrative user names
  $root_user_glob = regsubst($root_name_pattern, '^(.*)\\\0(.*)$', '\1*\2', 'IE')

  if !$pubkey and !$gssapi and !$password {
    fail('You would not be able to login to the server remotely. Enable at least one authentication method.')
  }

  if ($allow_root == 'password') and ($pubkey or $gssapi) {
    fail('Password authentication to root account is not available if better methods are.')
  }

  # check if a requested authentication method for root is generally enabled
  if ($allow_root == 'gssapi') and !$gssapi {
    fail('Root cannot login with GSSAPI if this method is generally disabled.')
  } elsif ($allow_root == 'pubkey') and !$pubkey {
    fail('Root cannot login with public key if this method is generally disabled.')
  } elsif ($allow_root == 'password') and !$password {
    fail('Root cannot login with a password if this method is generally disabled.')
  }

  # Sanity check for criteria matching in sftp settings hash.
  # Hash structure error are cought automatically by Puppet's strict typing.
  if $sftp_settings {
    $sftp_settings.each |$criteria, $settings| {
      $_criteria = split($criteria, ' ')
      if !($_criteria[0].downcase() in ['user', 'group', 'host', 'address']) {
        fail("You must specify a valid match criteria for sftp settings. Got '${criteria}' instead.")
      }
    }
  }

  # Install the openssh server package.
  package { 'openssh-server': ensure => present }

  file { '/etc/ssh/sshd_config':
    content => $custom_template ? {
      undef   => template('ssh/sshd_config.erb'),
      default => template( $custom_template ),
    },
    notify  => Service['ssh'],
  }

  # Ensure the daemon is running.
  service { 'ssh':
    ensure  => running,
    name    => $::osfamily ? {
      'Debian' => 'ssh',
      'RedHat' => 'sshd',
    },
    require => [Package['openssh-server'], File['/etc/ssh/sshd_config']]
  }

  $client_pkg = $::osfamily ? {
    'Debian' => 'openssh-client',
    'RedHat' => 'openssh-clients',
  }

  if $install_client {
    package { $client_pkg: ensure => present, }

    file { '/etc/ssh/ssh_config':
      content => template('ssh/ssh_config.erb'),
      require => Package[$client_pkg],
    }
  } else {
    package { $client_pkg: ensure => absent, }
  }
}


