# Changelog

## [v1.2] (https://code.stanford.edu/pe-public/ssh/-/tree/v1.0) (2023-06-29)

* Added a parameter for customization of global ssh client options.
* Added a fact returning currently installed version of openssh.

## [v1.0] (https://code.stanford.edu/pe-public/ssh/-/tree/v1.0) (2020-10-26)

* Initial release
